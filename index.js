const express = require('express')
var createError = require('http-errors');
const cors = require('cors');

const app = express()

var indexRouter = require('./routes/index')
var currentTrackRouter = require('./routes/current')

app.use(express.json());
app.use('/', indexRouter)
app.use('/current', currentTrackRouter);

var corsOptions = {
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200 
}
app.use(cors(corsOptions));

app.use(function(req, res, next) {
  next(createError(404));
});

app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = 'development';
  res.status(err.status || 500);
  res.json('error');
})

app.listen(3000, () => {
  console.log('listening on port 8000')
})
