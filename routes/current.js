var express = require('express');
var router = express.Router();

export const getCurrentTrack = () => {
  return {
    trackName: 'A Track Name',
    artistName: 'An Artist Name',
    imgUrl: 'https://picsum.photos/400/400',
    playing: true
  }
}

router.get('/', (req, res) => {
  console.log('Received request @ /current')
  const currentTrack = getCurrentTrack()
  return res.status(200).send({
    success: 'true',
    message: 'track',
    track: currentTrack
  })
})

module.exports = router;
